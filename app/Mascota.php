<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Mascota extends Model
{
    protected $table = 'mascotas';
    protected $fillable = ['nombre', 'raza', 'tipo', 'color', 'escala', 'escala_select', 'fecha', 'propietario', 'imagen_path', 'imagen_url', 'propietario_nombre'];


    public function getFechaAttribute($value)
    {

        return $value != '0000-00-00' ? (new Carbon($value))->format('d/m/y') : NULL;
    }
}
