<?php

namespace App\Http\Controllers;


use App\Mascota;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MascotaController extends Controller
{
    public function index(Request $request)
    {
        try {
            $mascotas = Mascota::where('nombre', 'LIKE', '%' . $request['buscar'] . '%')
                ->orWhere('raza', 'LIKE', '%' . $request['buscar'] . '%')
                ->orWhere('tipo', 'LIKE', '%' . $request['buscar'] . '%')
                ->orWhere('color', 'LIKE', '%' . $request['buscar'] . '%')
                ->orWhere('propietario_nombre', 'LIKE', '%' . $request['buscar'] . '%')
                ->orderBy('id', 'desc')
                ->paginate(5);

            return response()->json($mascotas);
        } catch (\Throwable $th) {
            return response()->json(["mensaje" => "Error al extraer los datos"], 500);
        }
    }
    public function store(Request $request)
    {
        try {
            $mascota = new Mascota;
            if ($request->hasFile("fileUpload") && $request->file('fileUpload')->isValid()) {
                $filename = time() . '.' . $request->fileUpload->getClientOriginalExtension();
                $request->fileUpload->move(public_path('storage'),  $filename);
                $path = 'storage/' . $filename;
                $mascota->imagen_path = $path;
                $mascota->imagen_url = $path;
            }
            $validator = Validator::make($request->all(), [
                'nombre' => 'required',
                'raza' => 'required',
                'tipo' => 'required',
                'color' => 'required',
                'escala' => 'required',
                'escala_select' => 'required',
            ],  [
                'required' => 'El campo es requerido.',
            ]);
            if ($validator->fails()) {
                return response()->json(["errors" => $validator->errors()], 403);
            }
            $mascota->nombre = $request->nombre;
            $mascota->raza = $request->raza;
            $mascota->tipo = $request->tipo;
            $mascota->color = $request->color;
            $mascota->escala = $request->escala;
            $mascota->escala_select = $request->escala_select;
            $mascota->fecha = $request->fecha;
            $mascota->propietario = $request->propietario ? 1 : 0;
            $mascota->propietario_nombre =  $request->propietario_nombre;

            $mascota->save();
            return response()->json('COMPLETE');
        } catch (\Throwable $th) {
            return response()->json(["mensaje" => "Error al Guardar los datos", $th], 500);
        }
    }
    public function update($id, Request $request)
    {
        try {

            $mascota = Mascota::find($id);
            if ($request->hasFile("fileUpload") && $request->file('fileUpload')->isValid()) {
                $filename = time() . '.' . $request->fileUpload->getClientOriginalExtension();
                $request->fileUpload->move(public_path('storage'),  $filename);
                $path = 'storage/' . $filename;
                $this->borrarImagen($mascota->imagen_path);
                $mascota->imagen_path = $path;
                $mascota->imagen_url = $path;
            }
            $validator = Validator::make($request->all(), [
                'nombre' => 'required',
                'raza' => 'required',
                'tipo' => 'required',
                'color' => 'required',
                'escala' => 'required',
                'escala_select' => 'required',
            ],  [
                'required' => 'El campo es requerido.',
            ]);
            if ($validator->fails()) {
                return response()->json(["errors" => $validator->errors()], 403);
            }
            $mascota->nombre = $request->nombre;
            $mascota->raza = $request->raza;
            $mascota->tipo = $request->tipo;
            $mascota->color = $request->color;
            $mascota->escala = $request->escala;
            $mascota->escala_select = $request->escala_select;
            $mascota->fecha = $request->fecha;
            $mascota->propietario = $request->propietario ? 1 : 0;
            $mascota->propietario_nombre =  $request->propietario_nombre;

            $mascota->save();
            return response()->json('COMPLETE');
        } catch (\Throwable $th) {
            return response()->json(["mensaje" => "Error al Actualizar los datos", $th], 500);
        }
    }
    public function delete($id)
    {
        try {
            $mascota = Mascota::find($id);
            if ($mascota->imagen_path != null && trim($mascota->imagen_path) != '')
                $this->borrarImagen($mascota->imagen_path);
            $mascota->delete();
            return response()->json('COMPLETE');
        } catch (\Throwable $th) {
            return response()->json(["mensaje" => "Error al Eliminar los datos", $th], 500);
        }
    }

    public function borrarImagen($path)
    {
        if (file_exists(public_path($path))) {
            $filedeleted = unlink(public_path($path));
            if ($filedeleted) {
                echo "File deleted";
            }
        } else {
            echo 'Unable to delete the given file';
        }
    }
}
