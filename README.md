# PruebaTecnicaELJ

# Configuracion de entorno

* se trabajo en laragon

* Version de php 7.2.5

* Version de nodejs 11.0.0
# Nota pueden instalar nvm para instalar versiones de node desde la terminal 
    https://4geeks.com/es/how-to/como-instalar-nvm-en-windows
# Comandos iniciales

* npm install 

* composer install

* php artisan migrate 

* php artisan storage:link

* npm run dev

# Anotaciones
* Tener creada la base de datos `pruebatecnicaelj`

* Recuerde tener previamente configurada la base de datos como se muestra:

# DB_CONNECTION=mysql
# DB_HOST=127.0.0.1
# DB_PORT=3306
# DB_DATABASE=pruebatecnicaelj
# DB_USERNAME=root
# DB_PASSWORD=

En caso de tener alguna configuracion personal, agregarla  en el archivo .env 
