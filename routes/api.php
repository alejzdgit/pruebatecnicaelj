<?php



Route::group(['middleware' => 'api', 'prefix' => 'mascotas'], function () {
    Route::get('/', 'MascotaController@index');
    Route::post('/', 'MascotaController@store');
    Route::put('/{id}', 'MascotaController@update');
    Route::delete('/{id}', 'MascotaController@delete');
});
