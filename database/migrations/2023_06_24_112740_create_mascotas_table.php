<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMascotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mascotas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('raza');
            $table->string('tipo');
            $table->string('color');
            $table->float('escala');
            $table->string('escala_select');
            $table->date('fecha')->nullable();
            $table->boolean('propietario')->default(false);
            $table->string('imagen_path')->nullable();
            $table->string('imagen_url')->nullable();
            $table->string('propietario_nombre')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mascotas');
    }
}
